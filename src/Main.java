import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Contact contact1 = new Contact("John Doe","+639123456789","Quezon City");
        System.out.println("--------------------");
        System.out.println(contact1.getName() + " has the following registered number:");
        System.out.println(contact1.getContactNumber());
        System.out.println(contact1.getName() + " has the following registered address:");
        System.out.println("My home is in " + contact1.getAddress());

        Contact contact2 = new Contact("Jane Doe","+639987654321","Caloocan City");
        System.out.println("--------------------");
        System.out.println(contact2.getName() + " has the following registered number:");
        System.out.println(contact2.getContactNumber());
        System.out.println(contact2.getName() + " has the following registered address:");
        System.out.println("My home is in " + contact2.getAddress());

        Phonebook phonebook = new Phonebook(new ArrayList<Contact>());

        phonebook.add(contact1);
        phonebook.add(contact2);

        System.out.println(phonebook.getContacts());

        if (phonebook.isEmpty()){
             System.out.println("The phonebook is currently empty");
        } else {
             System.out.println(phonebook.getContacts());
        }

    }
}