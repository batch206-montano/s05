import java.util.ArrayList;

public class Phonebook<contacts> extends Contact{


    ArrayList<Contact> contacts  = new ArrayList<>();

    public Phonebook(String name, String contactNumber, String address, ArrayList<Contact> contacts) {
        super(name, contactNumber, address);
        this.contacts = contacts;
    }

    public Phonebook(ArrayList<Contact> contacts) {
        super();
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void add(Contact phonebook) {
        this.contacts.add(phonebook);
    }

    public boolean isEmpty() {
        return false;
    }


}


